//
//  Model.swift
//  SkyLane
//
//  Created by Iain Attwater on 12/17/17.
//  Copyright © 2017 iconWare. All rights reserved.
//

import Foundation


struct Track: Decodable {
    let timestamp: Float?
    let latitude: Float?
    let longitude: Float?
    let groundspeed: Float?
    let altitude: Float?
}

struct Tracks: Decodable {
    let GetFlightTrackResult: [String:[Track]]
}
