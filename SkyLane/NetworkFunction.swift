//
//  NetworkFunction.swift
//  
//
//  Created by Iain Attwater on 12/17/17.
//

import Foundation
import Alamofire
class NetworkFunction {
    class func sendFlighttrackRequest() {
        /**
         FlightTrack
         get http://flightxml.flightaware.com/json/FlightXML3/GetFlightTrack
         */
        
        // Add Headers
        //let headers = [
        //]
        
        // Add URL parameters
        let urlParams = [
            "ident":"AAL69-1513319163-airline-0460",
            ]
        
        // Fetch Request
        Alamofire.request("https://flightxml.flightaware.com/json/FlightXML3/GetFlightTrack", method: .get, parameters: urlParams, headers: nil)
            .authenticate(user: "iaina", password: "9ebab0378b9dc0307ecd17801e1e0df4ce1f881c")
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    //debugPrint("HTTP Response Body: \(response.data)")
                    let track: Tracks = try! JSONDecoder().decode(Tracks.self, from: response.data!)
                    
                }
                else {
                    debugPrint("HTTP Request failed: \(response.result.error)")
                }
        }
    }
}
