//
//  FlightsTableViewController.swift
//  SkyLane
//
//  Created by Iain Attwater on 12/17/17.
//  Copyright © 2017 iconWare. All rights reserved.
//

import UIKit
import Alamofire
import MapKit

class FlightsTableViewController: UITableViewController {

    var track: Tracks?
    
    func sendFlighttrackRequest() {
        /**
         FlightTrack
         get http://flightxml.flightaware.com/json/FlightXML3/GetFlightTrack
         */
        
        // Add Headers
        //let headers = [
        //]
        
        // Add URL parameters
        let urlParams = [
            "ident":"DAL2056-1513319166-airline-0439",
            ]
        
        // Fetch Request
        Alamofire.request("https://flightxml.flightaware.com/json/FlightXML3/GetFlightTrack", method: .get, parameters: urlParams, headers: nil)
            .authenticate(user: "iaina", password: "9ebab0378b9dc0307ecd17801e1e0df4ce1f881c")
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    //debugPrint("HTTP Response Body: \(response.data)")
                    self.track = try! JSONDecoder().decode(Tracks.self, from: response.data!)
                    self.tableView.reloadData()
                }
                else {
                    debugPrint("HTTP Request failed: \(response.result.error)")
                }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.sendFlighttrackRequest()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.track?.GetFlightTrackResult.first?.value.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FlightsTableViewCell

        // Configure the cell...
        let d = self.track?.GetFlightTrackResult.first

        //cell.textLabel?.text = "\(String(describing: d?.value[indexPath.row].groundspeed))"
        cell.latitude.text = "\(d?.value[indexPath.row].latitude ?? 0)"
        cell.longitude.text = "\(d?.value[indexPath.row].longitude ?? 0)"
        cell.groundspeed.text = "\(d?.value[indexPath.row].groundspeed ?? 0)"
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let dvc  = segue.destination as! FlightMapViewController
        
        let v = Vehicle()
        
        let lat = track?.GetFlightTrackResult.first?.value[0].latitude
        let long = track?.GetFlightTrackResult.first?.value[0].longitude
        
        v.coordinate = CLLocationCoordinate2D(latitude: Double(lat!), longitude: Double(long!))
        v.title = "Test"
        v.subtitle = "Test Flight"
        
        dvc.vehicles.append(v)
        
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
