//
//  FlightsTableViewCell.swift
//  SkyLane
//
//  Created by Iain Attwater on 12/17/17.
//  Copyright © 2017 iconWare. All rights reserved.
//

import UIKit

class FlightsTableViewCell: UITableViewCell {
    @IBOutlet weak var latitude: UILabel!
    @IBOutlet weak var longitude: UILabel!
    @IBOutlet weak var groundspeed: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
