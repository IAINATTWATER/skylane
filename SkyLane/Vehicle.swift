//
//  Vehicle.swift
//  SkyLane
//
//  Created by Iain Attwater on 12/18/17.
//  Copyright © 2017 iconWare. All rights reserved.
//

import Foundation
import MapKit

class Vehicle: NSObject, MKAnnotation {
    
    var title: String?
    var subtitle: String?
    var coordinate = CLLocationCoordinate2D()
}
