//
//  FlightMapViewController.swift
//  SkyLane
//
//  Created by Iain Attwater on 12/18/17.
//  Copyright © 2017 iconWare. All rights reserved.
//

import UIKit
import MapKit

class FlightMapViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager =  CLLocationManager()
    var vehicles = [Vehicle]()
    
    
    
    func requestLocationAccess() {
        
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return
            
        case .denied, .restricted:
            print("Location denied")
            
        default:
            locationManager.requestWhenInUseAuthorization()
            
        }
    }
    
    func addAnnotations() {
        mapView.delegate = self
        mapView.addAnnotations(vehicles)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        requestLocationAccess()
        addAnnotations()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FlightMapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        else {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationView") ?? MKAnnotationView()
            
            annotationView.image = UIImage(named: "JetPlane")
            return annotationView
        }
    }
}


